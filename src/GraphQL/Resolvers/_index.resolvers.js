const { queryResolvers } = require("./query.resolvers");
const { mutationResolvers } = require("./mutation.resolvers");

const resolvers = {
  Query: queryResolvers,
  Mutation: mutationResolvers
};

module.exports = { resolvers };
