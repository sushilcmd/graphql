const { registerUser } = require("./../../modules/user/registerUser");

const mutationResolvers = {
  register: async function(_, args,context) {
    console.log(context);
    return registerUser(args["name"], args["email"]);
  }
};

module.exports = { mutationResolvers };
