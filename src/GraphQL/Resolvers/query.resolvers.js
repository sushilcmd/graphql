const { getUserProfile } = require("./../../modules/user/getUser");
const { getWallet } = require("./../../modules/wallet/getWallet");
const { getOrderBook } = require("./../../modules/public/getOrderBooks");

const queryResolvers = {
  profile: async function(_, __, { req }) {
    const xAuthKey = req.headers["x-auth-key"];
    return await getUserProfile(xAuthKey);
  },
  wallets: async function(_, params) {
    try {
      const coin = params.coin;
      if (params.coin) {
        const data = await getWallet(coin);
        return data;
      } else {
        const data = await getWallet();
        return data;
      }
    } catch (error) {
      console.log(error.message);
    }
  },
  orderBook: async function(_, params) {
    const { pair } = params;
    return await getOrderBook(pair);
  }
};

module.exports = { queryResolvers };
