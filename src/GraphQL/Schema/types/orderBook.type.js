const { gql } = require("apollo-server");

//GraphQL User Schema visible at GraphQL Endpoint
const typeOrderBook = gql`
  type BidsObject {
    price: Float,
    bidsvolume: Float,
    bidstotalvolume: Float
  }

  type AsksObject {
    price: Float,
    asksvolume: Float,
    askstotalvolume: Float
  }

  type OrderBook {
    asks: [AsksObject],
    bids: [BidsObject],
    msp: Float,
    mbp: Float,
    mp: Float
  }
`;

module.exports = { typeOrderBook };
// type MetaObject {
  //   status: Boolean!,
  //   msg: String!,
  //   code: String!
  // }