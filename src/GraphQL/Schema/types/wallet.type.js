const { gql } = require("apollo-server");

//GraphQL Wallet Schema visible at GraphQL Endpoint
const typeWallet = gql`
type Wallet {
  address:String
  availableBalance:Float!
  coinCode:String!
  isFreezed:Boolean
  inOrder:Float!
  isDeposit:Boolean!
  isWithdrawal:Boolean!
  noWithdrawReason:String
  ownerUserUid:String!
  totalBalance:Float!
  userEmailId:String!
  userIdx:Int!
  userName:String!
  walletType:String!
}

type Wallets {
  data: [Wallet]
}  
`;

module.exports = { typeWallet };
