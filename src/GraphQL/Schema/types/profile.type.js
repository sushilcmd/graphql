const { gql } = require("apollo-server");

//GraphQL Profile Schema visible at GraphQL Endpoint
const typeProfile = gql`
  type Profile {
    passwordUpdatedAt: Int,
    payFeeByBitrus: Boolean,
    profilePictureUrl: String,
    referralCode: String,
    referralEarnings: Float,
    referralLevel: Int,
    referredBy: String,
    referredByName: String,
    userEmailId: String,
    userName: String
  }
`;

module.exports = { typeProfile };
