const { typeWallet } = require("./types/wallet.type");
const { typeProfile } = require("./types/profile.type");
const { typeOrderBook } = require("./types/orderBook.type");

const types = [typeProfile, typeWallet, typeOrderBook];

module.exports = { types };
