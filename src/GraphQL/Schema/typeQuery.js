const { gql } = require("apollo-server");

//GraphQL Queries == Get Rest API
const typeQuery = gql`
  type Query {
    profile: Profile
    wallets(coin: String): Wallets
    orderBook(pair: String!): OrderBook
  }
`;

module.exports = { typeQuery };
