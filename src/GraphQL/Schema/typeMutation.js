const { gql } = require("apollo-server");

//GraphQL Queries == Get Rest API
const typeMutation = gql`
  type Mutation {
    register(name: String!, email: String!): RegisterResponse
  }

  type RegisterResponse {
    name: String!
    email: String!
  }
`;

module.exports = { typeMutation };
