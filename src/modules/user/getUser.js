const axios = require("axios");
const getUserProfile = async function(xAuthKey) {
  const { data } = await axios.get(`https://api.bitruslabs.com/v2/profile`, {
    headers: {
      "x-auth-key": xAuthKey
    }
  });
  return data.data;
};

module.exports = { getUserProfile };
