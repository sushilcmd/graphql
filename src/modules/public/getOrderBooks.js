const axios = require("axios");
const getOrderBook = async function(pair) {
  const { data } = await axios.get(`https://api.bitruslabs.com/v2/books?pair=${pair}`);
  return data.data;
};

module.exports = { getOrderBook };
