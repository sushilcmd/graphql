const axios = require("axios");
const getWallet = async function(coin=null) {
  let url = "https://api.bitruslabs.com/v2/wallets";
  if (coin) {
    url = url + `?coin=${coin}`;
  }
  const { data } = await axios.get(url, {
    headers: {
      "x-auth-key": "gjFepG3c12MuufsoPdNb2ktsC69koWwjvR/p"
    }
  });
  return data;
};

module.exports = { getWallet };
