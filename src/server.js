const { ApolloServer } = require("apollo-server");
const { typeDefs } = require("./GraphQL/Schema/schema");
const { resolvers } = require("./GraphQL/Resolvers/_index.resolvers");

const context = function(x) {
  return { req: x.req, res: x.res };
};

const server = new ApolloServer({ typeDefs, resolvers, context });
server.listen().then(({ url }) => console.log(`Server started at ${url}`));
